# vue-Grid 
It's css grid component.

## Install
`npm i @jcubegroup/vue-grid`

## Usage

```js
import Vue from 'vue'
import vGrid from '@jcubegroup/vue-grid'
Vue.use(vGrid)
```

```vue
<v-grid :cols="6" :rows="3" class="ui-gap-2">
    <v-col :col="4" :col-start="2">1</v-col>
    <v-col :row="2" :col-start="1" :col-end="3">2</v-col>
    <v-col :col="3" :col-end="7">3</v-col>
    <v-col :col-start="3" :col="'full'">4</v-col>
</v-grid>
```

[Full documentation](http://vue-grid.jcubegroup.com/guide/)