import commonjs from '@rollup/plugin-commonjs' // Конвертирование CommonJS модулей в ES6
import buble from '@rollup/plugin-buble' // Транспиляция/добавление полифилов для умеренной поддержки браузеров

import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload';
import fileSize from 'rollup-plugin-filesize';
import resolve from 'rollup-plugin-node-resolve';
import progress from 'rollup-plugin-progress';
import postcss from 'rollup-plugin-postcss';
import includePaths from 'rollup-plugin-includepaths';
import autoprefixer from 'autoprefixer';

let cache
const production = true;

export default {
  input: 'src/index.js', // Путь до относительного package.json
  output: {
    name: 'vueCssGrid',
    exports: 'named'
  },
  plugins: [
    progress({
      clearLine: true // default: true
    }),
    postcss({
      extract: true,
      minimize: production && true,
      use: ['sass'],
      plugins: [
        autoprefixer({
          browsers: [
            'Android >= 4.4',
            'BlackBerry >= 11',
            'Chrome >= 4',
            'Firefox >= 4',
            'Explorer >= 10',
            'iOS >= 4.1',
            'Opera >= 15',
            'Safari >= 4',
            'OperaMini >= 6',
            'ChromeAndroid >= 10',
            'FirefoxAndroid >= 4',
            'ExplorerMobile >= 10'
          ]
        })
      ]
    }),
    includePaths({
      paths: ['./src'], // Include Paths
      extensions: [
        '.js',
        '.scss'
      ]
    }),
    resolve(), // tells Rollup how to find node_modules
    commonjs(), // converts to ES modules
    buble(), // Alernate to Babel
    production && fileSize(), // Only run on production to increase bundle time
    !production && serve('dist'),   // index.html should be in dist
    !production && livereload()
  ],
}
