const setClass = (val, cls, prefix) => {
    for (const v in val) {
        const value = val[v]
        if (v === 'def') cls.push(`${prefix}${value}`)
        else cls.push(`${prefix}${value}@${v}`)
    }
}

const setVariable = (val, styles, variable) => {
    for (const v in val) {
        const value = val[v]
        if (v === 'def') styles.push(`--template: ${value};`)
        else styles.push(`--template-${v}: ${value};`)
    }
}

const vGrid = {
    render: function (h) {
        const {
            cols,
            colsSm,
            colsMd,
            colsLg,
            colsXl,
            colsXxl,

            rows,
            rowsSm,
            rowsMd,
            rowsLg,
            rowsXl,
            rowsXxl,

            gap,
            template,
            flow,
        } = this.$props

        let cls = [
            'ui-grid'
        ]
        let styles = []

        if (cols) {
            cls.push('ui-grid-cols')
            styles.push(`--cols: ${cols};`)
        }
        if (colsSm) {
            cls.push('ui-grid-cols-sm')
            styles.push(`--cols-sm: ${colsSm};`)
        }
        if (colsMd) {
            cls.push('ui-grid-cols-md')
            styles.push(`--cols-md: ${colsMd};`)
        }
        if (colsLg) {
            cls.push('ui-grid-cols-lg')
            styles.push(`--cols-lg: ${colsLg};`)
        }
        if (colsXl) {
            cls.push('ui-grid-cols-xl')
            styles.push(`--cols-xl: ${colsXl};`)
        }
        if (colsXxl) {
            cls.push('ui-grid-cols-xxl')
            styles.push(`--cols-xxl: ${colsXxl};`)
        }

        if (rows) {
            cls.push('ui-grid-rows')
            styles.push(`--rows: ${rows};`)
        }
        if (rowsSm) {
            cls.push('ui-grid-rows-sm')
            styles.push(`--rows-sm: ${rowsSm};`)
        }
        if (rowsMd) {
            cls.push('ui-grid-rows-md')
            styles.push(`--rows-md: ${rowsMd};`)
        }
        if (rowsLg) {
            cls.push('ui-grid-rows-lg')
            styles.push(`--rows-lg: ${rowsLg};`)
        }
        if (rowsXl) {
            cls.push('ui-grid-rows-xl')
            styles.push(`--rows-xl: ${rowsXl};`)
        }
        if (rowsXxl) {
            cls.push('ui-grid-rows-xxl')
            styles.push(`--rows-xxl: ${rowsXxl};`)
        }

        if (gap) cls.push(`ui-gap-${gap}`)
        if (template) {
            cls.push('ui-grid-template')
            cls.push(template)
        }
        if (flow) {
            cls.push(`ui-grid-flow-${flow}`)
        }

        return h(
            'div', {
                class: cls.join(' '),
                style: styles.join(' '),
            },
            this.$slots.default
        )
    },
    props: {
        cols: [String, Number],
        colsSm: [String, Number],
        colsMd: [String, Number],
        colsLg: [String, Number],
        colsXl: [String, Number],
        colsXxl: [String, Number],
        rows: [String, Number],
        rowsSm: [String, Number],
        rowsMd: [String, Number],
        rowsLg: [String, Number],
        rowsXl: [String, Number],
        rowsXxl: [String, Number],
        template: String,
        gap: [String, Number],
        flow: String,
    },
}
const vCol = {
    render: function (h) {
        const {
            name,
            col,
            colSm,
            colMd,
            colLg,
            colXl,
            colXxl,

            colStart,
            colStartSm,
            colStartMd,
            colStartLg,
            colStartXl,
            colStartXxl,

            colEnd,
            colEndSm,
            colEndMd,
            colEndLg,
            colEndXl,
            colEndXxl,

            row,
            rowSm,
            rowMd,
            rowLg,
            rowXl,
            rowXxl,

            rowStart,
            rowStartSm,
            rowStartMd,
            rowStartLg,
            rowStartXl,
            rowStartXxl,

            rowEnd,
            rowEndSm,
            rowEndMd,
            rowEndLg,
            rowEndXl,
            rowEndXxl,
        } = this.$props

        let cls = []
        let styles = []

        if (name) {
            cls.push('ui-grid-name')
            styles.push(`--name: ${name};`)
        }
        if (col) {
            if (col === 'hidden') {
                cls.push(`ui-grid-display-none`)
            } else {
                cls.push('ui-grid-col')
                cls.push(`ui-grid-display-block`)
                styles.push(`--col: ${col};`)
            }
        }
        if (colSm) {
            if (colSm === 'hidden') {
                cls.push(`ui-grid-display-none-sm`)
            } else {
                cls.push('ui-grid-col-sm')
                cls.push(`ui-grid-display-block-sm`)
                styles.push(`--col-sm: ${colSm};`)
            }
        }
        if (colMd) {
            if (colMd === 'hidden') {
                cls.push(`ui-grid-display-none-md`)
            } else {
                cls.push('ui-grid-col-md')
                cls.push(`ui-grid-display-block-md`)
                styles.push(`--col-md: ${colMd};`)
            }
        }
        if (colLg) {
            if (colLg === 'hidden') {
                cls.push(`ui-grid-display-none-lg`)
            } else {
                cls.push('ui-grid-col-lg')
                cls.push(`ui-grid-display-block-lg`)
                styles.push(`--col-lg: ${colLg};`)
            }
        }
        if (colXl) {
            if (colXl === 'hidden') {
                cls.push(`ui-grid-display-none-xl`)
            } else {
                cls.push('ui-grid-col-xl')
                cls.push(`ui-grid-display-block-xl`)
                styles.push(`--col-xl: ${colXl};`)
            }
        }
        if (colXxl) {
            if (colXxl === 'hidden') {
                cls.push(`ui-grid-display-none-xxl`)
            } else {
                cls.push('ui-grid-col-xxl')
                cls.push(`ui-grid-display-block-xxl`)
                styles.push(`--col-xxl: ${colXxl};`)
            }
        }

        if (colStart) {
            cls.push('ui-grid-col-start')
            styles.push(`--col-start: ${colStart};`)
        }
        if (colStartSm) {
            cls.push('ui-grid-col-start-sm')
            styles.push(`--col-start-sm: ${colStartSm};`)
        }
        if (colStartMd) {
            cls.push('ui-grid-col-start-md')
            styles.push(`--col-start-md: ${colStartMd};`)
        }
        if (colStartLg) {
            cls.push('ui-grid-col-start-lg')
            styles.push(`--col-start-lg: ${colStartLg};`)
        }
        if (colStartXl) {
            cls.push('ui-grid-col-start-xl')
            styles.push(`--col-start-xl: ${colStartXl};`)
        }
        if (colStartXxl) {
            cls.push('ui-grid-col-start-xxl')
            styles.push(`--col-start-xxl: ${colStartXxl};`)
        }

        if (colEnd) {
            cls.push('ui-grid-col-end')
            styles.push(`--col-end: ${colEnd};`)
        }
        if (colEndSm) {
            cls.push('ui-grid-col-end-sm')
            styles.push(`--col-end-sm: ${colEndSm};`)
        }
        if (colEndMd) {
            cls.push('ui-grid-col-end-md')
            styles.push(`--col-end-md: ${colEndMd};`)
        }
        if (colEndLg) {
            cls.push('ui-grid-col-end-lg')
            styles.push(`--col-end-lg: ${colEndLg};`)
        }
        if (colEndXl) {
            cls.push('ui-grid-col-end-xl')
            styles.push(`--col-end-xl: ${colEndXl};`)
        }
        if (colEndXxl) {
            cls.push('ui-grid-col-end-xxl')
            styles.push(`--col-end-xxl: ${colEndXxl};`)
        }

        if (row) {
            cls.push('ui-grid-row')
            styles.push(`--row: ${row};`)
        }
        if (rowSm) {
            cls.push('ui-grid-row-sm')
            styles.push(`--row-sm: ${rowSm};`)
        }
        if (rowMd) {
            cls.push('ui-grid-row-md')
            styles.push(`--row-md: ${rowMd};`)
        }
        if (rowLg) {
            cls.push('ui-grid-row-lg')
            styles.push(`--row-lg: ${rowLg};`)
        }
        if (rowXl) {
            cls.push('ui-grid-row-xl')
            styles.push(`--row-xl: ${rowXl};`)
        }
        if (rowXxl) {
            cls.push('ui-grid-row-xxl')
            styles.push(`--row-xxl: ${rowXxl};`)
        }

        if (rowStart) {
            cls.push('ui-grid-row-start')
            styles.push(`--row-start: ${rowStart};`)
        }
        if (rowStartSm) {
            cls.push('ui-grid-row-start-sm')
            styles.push(`--row-start-sm: ${rowStartSm};`)
        }
        if (rowStartMd) {
            cls.push('ui-grid-row-start-md')
            styles.push(`--row-start-md: ${rowStartMd};`)
        }
        if (rowStartLg) {
            cls.push('ui-grid-row-start-lg')
            styles.push(`--row-start-lg: ${rowStartLg};`)
        }
        if (rowStartXl) {
            cls.push('ui-grid-row-start-xl')
            styles.push(`--row-start-xl: ${rowStartXl};`)
        }
        if (rowStartXxl) {
            cls.push('ui-grid-row-start-xxl')
            styles.push(`--row-start-xxl: ${rowStartXxl};`)
        }

        if (rowEnd) {
            cls.push('ui-grid-row-end')
            styles.push(`--row-end: ${rowEnd};`)
        }
        if (rowEndSm) {
            cls.push('ui-grid-row-end-sm')
            styles.push(`--row-end-sm: ${rowEndSm};`)
        }
        if (rowEndMd) {
            cls.push('ui-grid-row-end-md')
            styles.push(`--row-end-md: ${rowEndMd};`)
        }
        if (rowEndLg) {
            cls.push('ui-grid-row-end-lg')
            styles.push(`--row-end-lg ${rowEndLg};`)
        }
        if (rowEndXl) {
            cls.push('ui-grid-row-end-xl')
            styles.push(`--row-end-xl ${rowEndXl};`)
        }
        if (rowEndXxl) {
            cls.push('ui-grid-row-end-xxl')
            styles.push(`--row-end-xxl ${rowEndXxl};`)
        }

        return h(
            'div', {
                class: cls.join(' '),
                style: styles.join(' '),
            },
            this.$slots.default
        )
    },
    props: {
        name: String,
        col: [String, Number],
        colSm: [String, Number],
        colMd: [String, Number],
        colLg: [String, Number],
        colXl: [String, Number],
        colXxl: [String, Number],
        colStart: [String, Number],
        colStartSm: [String, Number],
        colStartMd: [String, Number],
        colStartLg: [String, Number],
        colStartXl: [String, Number],
        colStartXxl: [String, Number],
        colEnd: [String, Number],
        colEndSm: [String, Number],
        colEndMd: [String, Number],
        colEndLg: [String, Number],
        colEndXl: [String, Number],
        colEndXxl: [String, Number],
        row: [String, Number],
        rowSm: [String, Number],
        rowMd: [String, Number],
        rowLg: [String, Number],
        rowXl: [String, Number],
        rowXxl: [String, Number],
        rowStart: [String, Number],
        rowStartSm: [String, Number],
        rowStartMd: [String, Number],
        rowStartLg: [String, Number],
        rowStartXl: [String, Number],
        rowStartXxl: [String, Number],
        rowEnd: [String, Number],
        rowEndSm: [String, Number],
        rowEndMd: [String, Number],
        rowEndLg: [String, Number],
        rowEndXl: [String, Number],
        rowEndXxl: [String, Number],
    },
}

export {vGrid, vCol}