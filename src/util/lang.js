export function each (obj, cb) {
    for (const key in obj) {
        if (cb.call(obj[key], obj[key], key) === false) {
            break
        }
    }
}