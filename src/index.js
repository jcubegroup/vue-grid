import {each} from './util/lang'
import * as components from './library/components.js'
import './scss/main.scss'

const vueGrid = {
    components,

    install(Vue) {
        each(components, (def, name) => {
            Vue.component(`${name}`, def)
        })
    }
}

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(vueGrid)
}

export default vueGrid
